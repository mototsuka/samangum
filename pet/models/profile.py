from sqlalchemy import Column, String, text, Integer
from sqlalchemy.dialects.mysql import TIMESTAMP as Timestamp
from pet.db.client import Base


class TimestampMixin(object):
    created_at = Column(Timestamp, nullable=False,
                        server_default=text('current_timestamp'))
    updated_at = Column(Timestamp, nullable=False,
                        server_default=text('current_timestamp on update current_timestamp'))


class Profile(Base, TimestampMixin):
    __tablename__ = 'profile'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(128), nullable=False)
    bio = Column(String(128), nullable=False)
    image = Column(String(128), nullable=True)
