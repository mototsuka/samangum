import os
from fastapi import APIRouter
from pydantic import ValidationError
from pet.routers.v1 import profile
from pet.schemas.response import ResponseJson

api_router = APIRouter()

if os.environ.get("ENV") == "LOCAL":
    api_router.include_router(profile.router)


@api_router.get("/")
async def root() -> ResponseJson:
    try:
        response = ResponseJson(
            message="Hello Animals",
        )
    except ValidationError as _e:
        response = ResponseJson()

    return response
