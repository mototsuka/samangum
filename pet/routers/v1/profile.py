from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from pet.schemas.profile import Profile
from pet.db.client import get_db
import pet.db.crud as crud

router = APIRouter()


@router.get("/profile", response_model=Profile)
async def get_profile(name: str, db: Session = Depends(get_db)):
    return crud.read_profile(name=name, db=db)
