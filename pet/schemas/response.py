from pydantic import BaseModel


class ResponseJson(BaseModel):
    message: str = ""
