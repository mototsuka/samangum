from pydantic import BaseModel


class Profile(BaseModel):
    name: str
    bio: str = ""

    class Config:
        orm_mode = True
