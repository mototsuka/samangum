import uvicorn
from mangum import Mangum
from fastapi import FastAPI
from pet.routers.api import api_router


app = FastAPI()
app.include_router(api_router)


lambda_handler = Mangum(app)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8080, log_level="info")
