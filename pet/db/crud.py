from fastapi import HTTPException
from sqlalchemy.orm import Session
from starlette.status import HTTP_404_NOT_FOUND
from pet.models.profile import Profile


def read_profile(db: Session, name: str):
    try:
        item = db.query(Profile).filter_by(name=name).order_by(Profile.id).first()
    except BaseException as _e:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail='Record not found.'
        )

    return item
