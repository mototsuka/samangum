from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session
import os


if os.environ.get("ENV") == "local":
    DATABASE = "mysql://%s@%s:%s/%s?charset=utf8" % (
        os.environ.get("MYSQL_ROOT"),
        os.environ.get("MYSQL_HOST"),
        os.environ.get("MYSQL_PORT"),
        os.environ.get("MYSQL_DATABASE"),
    )
else:
    DATABASE = "mysql://%s@%s:%s/%s?charset=utf8" % (
        "dummy",
        "dummy",
        "8888",
        "dummy",
    )

engine = create_engine(
    DATABASE,
    encoding='utf-8',
    echo=True
)

SessionLocal = scoped_session(
    sessionmaker(
        autocommit=False,
        autoflush=False,
        bind=engine
    )
)

Base = declarative_base()
Base.query = SessionLocal.query_property()


def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()
